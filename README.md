# emp-tool-rust

Rust port of emp-tool

I want to learn rust this summer. I believe it is future of cryptography, so I have decided to try to port a popular library: emp-tool
https://github.com/emp-toolkit/emp-tool
There are a lot of moving parts. Netio, operator overloading, AES-NI, SSE2-4 vector stuff. 